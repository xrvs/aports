# Contributor: Leo <thinkabit.ukim@gmail.com>
# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=libical
pkgver=3.0.15
pkgrel=0
pkgdesc="Reference implementation of the iCalendar format"
url="https://libical.github.io/libical/"
arch="all"
license="LGPL-2.1-only OR MPL-2.0"
depends_dev="libxml2-dev gobject-introspection-dev"
makedepends="perl cmake vala glib-dev icu-dev samurai $depends_dev"
checkdepends="tzdata py3-gobject3"
subpackages="$pkgname-dev"
source="https://github.com/libical/libical/releases/download/v$pkgver/libical-$pkgver.tar.gz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DSHARED_ONLY=true \
		-DENABLE_GTK_DOC=false \
		-DGOBJECT_INTROSPECTION=true \
		-DICAL_GLIB_VAPI=true
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E '(icalrecurtest|icalrecurtest_r)'
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
14f0eaabdd8fc56d91d2fff8647b3871439cceae3cb7af31eaae30b3cc29f818b6f9d582dd4770b8b3b0c6fe53684258d30c743a5fd5dd337fda64e3adae35ba  libical-3.0.15.tar.gz
"
