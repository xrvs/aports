# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=libharu
pkgver=2.4.2
pkgrel=0
pkgdesc="C library for generating PDF documents"
url="https://github.com/libharu/libharu"
arch="all"
license="custom"
depends_dev="libpng-dev"
makedepends="$depends_dev cmake samurai"
subpackages="$pkgname-dev $pkgname-doc"
source="https://github.com/libharu/libharu/archive/v$pkgver/libharu-$pkgver.tar.gz
	soversion.patch
	"
options="!check" # no tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DLIBHPDF_STATIC=OFF
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build

	install -Dm644 LICENSE "$pkgdir/usr/share/licenses/libharu/LICENSE"

	# remove useless files
	rm -rf "$pkgdir"/usr/share/libharu
}

sha512sums="
faa5c0390f22ae8bbe3dbc2b49e49b475257a70c5772e456cc34df3f98b26e39b65b17b0f982dd844b9774a4217a621337a1a8a810d7be09569a7bffe4ea1f15  libharu-2.4.2.tar.gz
8e65e377c17a6c2471c1302ff3be9f1e685ae5a63e202aff16006e2560046603e770a640f4b0f25046573e54e75142b7546393bd0cfa4e9da38e9aa3053f848d  soversion.patch
"
