# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=httm
pkgver=0.15.1
pkgrel=0
pkgdesc="Interactive, file-level Time Machine-like tool for ZFS/btrfs"
url="https://github.com/kimono-koans/httm"
arch="aarch64 armhf armv7 ppc64le x86 x86_64"  # blocked by rust/cargo
license="MPL-2.0"
makedepends="cargo"
subpackages="$pkgname-doc"
source="https://github.com/kimono-koans/httm/archive/refs/tags/$pkgver/httm-$pkgver.tar.gz"
options="!check"  # no tests provided

export CARGO_PROFILE_RELEASE_CODEGEN_UNITS=1
export CARGO_PROFILE_RELEASE_LTO="true"
export CARGO_PROFILE_RELEASE_OPT_LEVEL="s"
export CARGO_PROFILE_RELEASE_PANIC="abort"

prepare() {
	default_prepare

	cargo fetch --locked
}

build() {
	cargo build --frozen --release
}

package() {
	install -D -m755 target/release/$pkgname -t "$pkgdir"/usr/bin/
	install -D -m644 $pkgname.1 -t "$pkgdir"/usr/share/man/man1/
}

sha512sums="
66c116d9b89580517c94542ea57ad527953cf8c28490ca5b26faa842dd8738747127f1afd6a49bc465a751481999896fe1a406b2c81cd3d3d108f4e22a4fbdbb  httm-0.15.1.tar.gz
"
