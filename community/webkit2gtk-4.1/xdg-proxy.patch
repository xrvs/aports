From f6198cdad380b68e02c7a135b18fcf7e387078ea Mon Sep 17 00:00:00 2001
From: Carlos Garcia Campos <cgarcia@igalia.com>
Date: Fri, 9 Sep 2022 01:04:05 -0700
Subject: [PATCH] [GTK][WPE] Use a single xdg-dbus-proxy process
 https://bugs.webkit.org/show_bug.cgi?id=244930

Reviewed by Michael Catanzaro.

We currently spawn two xdg-dbus-proxy processes, one for the session bus
and one for the atspi bus, but we can do both with a single process.

* Source/WebKit/UIProcess/Launcher/glib/BubblewrapLauncher.cpp:
(WebKit::bindDBusSession): Receive the XDGDBusProxy as argument. Use the
new api to get the proxy paths.
(WebKit::bindA11y): Ditto.
(WebKit::bubblewrapSpawn): Create a single XDGDBusProxy and pass it to bindDBusSession() and bindA11y().
* Source/WebKit/UIProcess/Launcher/glib/XDGDBusProxy.cpp:
(WebKit::XDGDBusProxy::makePath): Helper to create a path for the given DBus address.
(WebKit::XDGDBusProxy::makeProxy): Helper to create the proxy file using the given template.
(WebKit::XDGDBusProxy::dbusSessionProxy): Return the proxy paths for DBus session.
(WebKit::XDGDBusProxy::accessibilityProxy): Return the proxy paths for atspi.
(WebKit::XDGDBusProxy::launch): Return falseif we don't need to spawn xdg-dbus-proxy at all.
(WebKit::XDGDBusProxy::XDGDBusProxy): Deleted.
(WebKit::XDGDBusProxy::~XDGDBusProxy): Deleted.
(WebKit::XDGDBusProxy::makeProxy const): Deleted.
(WebKit::XDGDBusProxy::launch const): Deleted.
* Source/WebKit/UIProcess/Launcher/glib/XDGDBusProxy.h:
(WebKit::XDGDBusProxy::proxyPath const): Deleted.
(WebKit::XDGDBusProxy::path const): Deleted.
(): Deleted.

Canonical link: https://commits.webkit.org/254293@main
---
 .../Launcher/glib/BubblewrapLauncher.cpp      |  45 ++--
 .../UIProcess/Launcher/glib/XDGDBusProxy.cpp  | 204 +++++++++---------
 .../UIProcess/Launcher/glib/XDGDBusProxy.h    |  31 +--
 3 files changed, 147 insertions(+), 133 deletions(-)

diff --git a/Source/WebKit/UIProcess/Launcher/glib/BubblewrapLauncher.cpp b/Source/WebKit/UIProcess/Launcher/glib/BubblewrapLauncher.cpp
index 3919f1bd6d11..de8d5f1ca5cb 100644
--- a/Source/WebKit/UIProcess/Launcher/glib/BubblewrapLauncher.cpp
+++ b/Source/WebKit/UIProcess/Launcher/glib/BubblewrapLauncher.cpp
@@ -197,15 +197,17 @@ static void bindIfExists(Vector<CString>& args, const char* path, BindFlags bind
         args.appendVector(Vector<CString>({ bindType, path, path }));
 }
 
-static void bindDBusSession(Vector<CString>& args, bool allowPortals)
+static void bindDBusSession(Vector<CString>& args, XDGDBusProxy& dbusProxy, bool allowPortals)
 {
-    static std::unique_ptr<XDGDBusProxy> proxy = makeUnique<XDGDBusProxy>(XDGDBusProxy::Type::SessionBus, allowPortals);
+    auto dbusSessionProxy = dbusProxy.dbusSessionProxy(allowPortals ? XDGDBusProxy::AllowPortals::Yes : XDGDBusProxy::AllowPortals::No);
+    if (!dbusSessionProxy)
+        return;
 
-    if (!proxy->proxyPath().isNull() && !proxy->path().isNull()) {
-        args.appendVector(Vector<CString>({
-            "--bind", proxy->proxyPath(), proxy->path(),
-        }));
-    }
+    GUniquePtr<char> proxyAddress(g_strdup_printf("unix:path=%s", dbusSessionProxy->second.data()));
+    args.appendVector(Vector<CString> {
+        "--ro-bind", WTFMove(dbusSessionProxy->first), WTFMove(dbusSessionProxy->second),
+        "--setenv", "DBUS_SESSION_BUS_ADDRESS", proxyAddress.get()
+    });
 }
 
 #if PLATFORM(X11)
@@ -348,17 +350,17 @@ static void bindGtkData(Vector<CString>& args)
 #endif
 
 #if ENABLE(ACCESSIBILITY)
-static void bindA11y(Vector<CString>& args)
+static void bindA11y(Vector<CString>& args, XDGDBusProxy& dbusProxy)
 {
-    static std::unique_ptr<XDGDBusProxy> proxy = makeUnique<XDGDBusProxy>(XDGDBusProxy::Type::AccessibilityBus);
-
-    if (!proxy->proxyPath().isNull()) {
-        GUniquePtr<char> proxyAddress(g_strdup_printf("unix:path=%s", proxy->proxyPath().data()));
-        args.appendVector(Vector<CString> {
-            "--ro-bind", proxy->proxyPath(), proxy->proxyPath(),
-            "--setenv", "AT_SPI_BUS_ADDRESS", proxyAddress.get(),
-        });
-    }
+    auto accessibilityProxy = dbusProxy.accessibilityProxy();
+    if (!accessibilityProxy)
+        return;
+
+    GUniquePtr<char> proxyAddress(g_strdup_printf("unix:path=%s", accessibilityProxy->second.data()));
+    args.appendVector(Vector<CString> {
+        "--ro-bind", WTFMove(accessibilityProxy->first), WTFMove(accessibilityProxy->second),
+        "--setenv", "AT_SPI_BUS_ADDRESS", proxyAddress.get()
+    });
 }
 #endif
 
@@ -753,7 +755,9 @@ GRefPtr<GSubprocess> bubblewrapSpawn(GSubprocessLauncher* launcher, const Proces
                 sandboxArgs.appendVector(Vector<CString>({ "--bind-try", extraPath.utf8(), extraPath.utf8() }));
         }
 
-        bindDBusSession(sandboxArgs, flatpakInfoFd != -1);
+        static std::unique_ptr<XDGDBusProxy> dbusProxy = makeUnique<XDGDBusProxy>();
+        if (dbusProxy)
+            bindDBusSession(sandboxArgs, *dbusProxy, flatpakInfoFd != -1);
         // FIXME: We should move to Pipewire as soon as viable, Pulse doesn't restrict clients atm.
         bindPulse(sandboxArgs);
         bindSndio(sandboxArgs);
@@ -763,11 +767,14 @@ GRefPtr<GSubprocess> bubblewrapSpawn(GSubprocessLauncher* launcher, const Proces
         // FIXME: This is also fixed by Pipewire once in use.
         bindV4l(sandboxArgs);
 #if ENABLE(ACCESSIBILITY)
-        bindA11y(sandboxArgs);
+        if (dbusProxy)
+            bindA11y(sandboxArgs, *dbusProxy);
 #endif
 #if PLATFORM(GTK)
         bindGtkData(sandboxArgs);
 #endif
+        if (dbusProxy && !dbusProxy->launch())
+            dbusProxy = nullptr;
     } else {
         // Only X11 users need this for XShm which is only the Web process.
         sandboxArgs.append("--unshare-ipc");
diff --git a/Source/WebKit/UIProcess/Launcher/glib/XDGDBusProxy.cpp b/Source/WebKit/UIProcess/Launcher/glib/XDGDBusProxy.cpp
index c8ac9b6661d8..c9e0b1f17cf0 100644
--- a/Source/WebKit/UIProcess/Launcher/glib/XDGDBusProxy.cpp
+++ b/Source/WebKit/UIProcess/Launcher/glib/XDGDBusProxy.cpp
@@ -41,51 +41,24 @@
 
 namespace WebKit {
 
-XDGDBusProxy::XDGDBusProxy(Type type, bool allowPortals)
-    : m_type(type)
+CString XDGDBusProxy::makePath(const char* dbusAddress)
 {
-    switch (m_type) {
-    case Type::SessionBus:
-        m_dbusAddress = g_getenv("DBUS_SESSION_BUS_ADDRESS");
-        break;
-    case Type::AccessibilityBus:
-#if ENABLE(ACCESSIBILITY)
-        m_dbusAddress = WebCore::PlatformDisplay::sharedDisplay().accessibilityBusAddress().utf8();
-#endif
-        break;
-    }
-
-    if (m_dbusAddress.isNull() || !g_str_has_prefix(m_dbusAddress.data(), "unix:"))
-        return;
-
-    m_proxyPath = makeProxy();
-    if (m_proxyPath.isNull())
-        return;
-
-#if USE(ATSPI)
-    if (m_type == Type::AccessibilityBus)
-        WebCore::PlatformDisplay::sharedDisplay().setAccessibilityBusAddress(makeString("unix:path=", m_proxyPath.data()));
-#endif
+    if (!dbusAddress || !g_str_has_prefix(dbusAddress, "unix:"))
+        return { };
 
-    if (const char* path = strstr(m_dbusAddress.data(), "path=")) {
+    if (const char* path = strstr(dbusAddress, "path=")) {
         path += strlen("path=");
         const char* pathEnd = path;
         while (*pathEnd && *pathEnd != ',')
             pathEnd++;
 
-        m_path = CString(path, pathEnd - path);
+        return CString(path, pathEnd - path);
     }
 
-    m_syncFD = launch(allowPortals);
+    return { };
 }
 
-XDGDBusProxy::~XDGDBusProxy()
-{
-    if (m_syncFD != -1)
-        close(m_syncFD);
-}
-
-CString XDGDBusProxy::makeProxy() const
+CString XDGDBusProxy::makeProxy(const char* proxyTemplate)
 {
     GUniquePtr<char> appRunDir(g_build_filename(g_get_user_runtime_dir(), BASE_DIRECTORY, nullptr));
     if (g_mkdir_with_parents(appRunDir.get(), 0700) == -1) {
@@ -93,86 +66,117 @@ CString XDGDBusProxy::makeProxy() const
         return { };
     }
 
-    auto proxyTemplate = [](Type type) -> const char* {
-        switch (type) {
-        case Type::SessionBus:
-            return "bus-proxy-XXXXXX";
-        case Type::AccessibilityBus:
-            return "a11y-proxy-XXXXXX";
-        }
-        RELEASE_ASSERT_NOT_REACHED();
-    };
-
-    GUniquePtr<char> proxySocketTemplate(g_build_filename(appRunDir.get(), proxyTemplate(m_type), nullptr));
-    int fd;
-    if ((fd = g_mkstemp(proxySocketTemplate.get())) == -1) {
+    GUniquePtr<char> proxySocketTemplate(g_build_filename(appRunDir.get(), proxyTemplate, nullptr));
+    UnixFileDescriptor fd(g_mkstemp(proxySocketTemplate.get()), UnixFileDescriptor::Adopt);
+    if (!fd) {
         g_warning("Failed to make socket file %s for dbus proxy: %s", proxySocketTemplate.get(), g_strerror(errno));
         return { };
     }
 
-    close(fd);
     return proxySocketTemplate.get();
 }
 
-int XDGDBusProxy::launch(bool allowPortals) const
+std::optional<std::pair<CString, CString>> XDGDBusProxy::dbusSessionProxy(AllowPortals allowPortals)
 {
-    int syncFds[2];
-    if (pipe(syncFds) == -1)
-        g_error("Failed to make syncfds for dbus-proxy: %s", g_strerror(errno));
-    setCloseOnExec(syncFds[0]);
+    if (!m_dbusSessionProxyPath.isNull())
+        return std::pair<CString, CString> { m_dbusSessionProxyPath, m_dbusSessionPath };
+
+    const char* dbusAddress = g_getenv("DBUS_SESSION_BUS_ADDRESS");
+    m_dbusSessionPath = makePath(dbusAddress);
+    if (m_dbusSessionPath.isNull())
+        return std::nullopt;
+
+    m_dbusSessionProxyPath = makeProxy("bus-proxy-XXXXXX");
+    if (m_dbusSessionProxyPath.isNull()) {
+        m_dbusSessionPath = { };
+        return std::nullopt;
+    }
 
-    GUniquePtr<char> syncFdStr(g_strdup_printf("--fd=%d", syncFds[1]));
-    Vector<CString> proxyArgs = {
-        m_dbusAddress, m_proxyPath,
+    m_args.appendVector(Vector<CString> {
+        CString(dbusAddress), m_dbusSessionProxyPath,
         "--filter",
-        syncFdStr.get()
-    };
-
-    auto enableLogging = [](Type type) -> bool {
-        switch (type) {
-        case Type::SessionBus:
-            return !g_strcmp0(g_getenv("WEBKIT_ENABLE_DBUS_PROXY_LOGGING"), "1");
-        case Type::AccessibilityBus:
-            return !g_strcmp0(g_getenv("WEBKIT_ENABLE_A11Y_DBUS_PROXY_LOGGING"), "1");
-        }
-        RELEASE_ASSERT_NOT_REACHED();
-    };
-    if (enableLogging(m_type))
-        proxyArgs.append("--log");
+        // GStreamers plugin install helper.
+        "--call=org.freedesktop.PackageKit=org.freedesktop.PackageKit.Modify2.InstallGStreamerResources@/org/freedesktop/PackageKit"
+    });
 
-    switch (m_type) {
-    case Type::SessionBus: {
 #if ENABLE(MEDIA_SESSION)
-        if (auto* app = g_application_get_default()) {
-            if (const char* appID = g_application_get_application_id(app)) {
-                auto mprisSessionID = makeString("--own=org.mpris.MediaPlayer2.", appID, ".*");
-                proxyArgs.append(mprisSessionID.ascii().data());
-            }
+    if (auto* app = g_application_get_default()) {
+        if (const char* appID = g_application_get_application_id(app)) {
+            auto mprisSessionID = makeString("--own=org.mpris.MediaPlayer2.", appID, ".*");
+            m_args.append(mprisSessionID.ascii().data());
         }
+    }
 #endif
-        // GStreamers plugin install helper.
-        proxyArgs.append("--call=org.freedesktop.PackageKit=org.freedesktop.PackageKit.Modify2.InstallGStreamerResources@/org/freedesktop/PackageKit");
 
-        if (allowPortals)
-            proxyArgs.append("--talk=org.freedesktop.portal.Desktop");
-        break;
-    }
-    case Type::AccessibilityBus:
+    if (allowPortals == AllowPortals::Yes)
+        m_args.append("--talk=org.freedesktop.portal.Desktop");
+
+    if (!g_strcmp0(g_getenv("WEBKIT_ENABLE_DBUS_PROXY_LOGGING"), "1"))
+        m_args.append("--log");
+
+    return std::pair<CString, CString> { m_dbusSessionProxyPath, m_dbusSessionPath };
+}
+
+std::optional<std::pair<CString, CString>> XDGDBusProxy::accessibilityProxy()
+{
 #if ENABLE(ACCESSIBILITY)
-        proxyArgs.appendVector(Vector<CString> {
-            "--sloppy-names",
-            "--call=org.a11y.atspi.Registry=org.a11y.atspi.Socket.Embed@/org/a11y/atspi/accessible/root",
-            "--call=org.a11y.atspi.Registry=org.a11y.atspi.Socket.Unembed@/org/a11y/atspi/accessible/root",
-            "--call=org.a11y.atspi.Registry=org.a11y.atspi.Registry.GetRegisteredEvents@/org/a11y/atspi/registry",
-            "--call=org.a11y.atspi.Registry=org.a11y.atspi.DeviceEventController.GetKeystrokeListeners@/org/a11y/atspi/registry/deviceeventcontroller",
-            "--call=org.a11y.atspi.Registry=org.a11y.atspi.DeviceEventController.GetDeviceEventListeners@/org/a11y/atspi/registry/deviceeventcontroller",
-            "--call=org.a11y.atspi.Registry=org.a11y.atspi.DeviceEventController.NotifyListenersSync@/org/a11y/atspi/registry/deviceeventcontroller",
-            "--call=org.a11y.atspi.Registry=org.a11y.atspi.DeviceEventController.NotifyListenersAsync@/org/a11y/atspi/registry/deviceeventcontroller",
-        });
-#endif
-        break;
+    if (!m_accessibilityProxyPath.isNull())
+        return std::pair<CString, CString> { m_accessibilityProxyPath, m_accessibilityPath };
+
+    auto dbusAddress = WebCore::PlatformDisplay::sharedDisplay().accessibilityBusAddress().utf8();
+    m_accessibilityPath = makePath(dbusAddress.data());
+    if (m_accessibilityPath.isNull())
+        return std::nullopt;
+
+    m_accessibilityProxyPath = makeProxy("a11y-proxy-XXXXXX");
+    if (m_accessibilityProxyPath.isNull()) {
+        m_accessibilityPath = { };
+        return std::nullopt;
     }
 
+#if USE(ATSPI)
+    WebCore::PlatformDisplay::sharedDisplay().setAccessibilityBusAddress(makeString("unix:path=", m_accessibilityPath.data()));
+#endif
+
+    m_args.appendVector(Vector<CString> {
+        WTFMove(dbusAddress), m_accessibilityProxyPath,
+        "--filter",
+        "--sloppy-names",
+        "--call=org.a11y.atspi.Registry=org.a11y.atspi.Socket.Embed@/org/a11y/atspi/accessible/root",
+        "--call=org.a11y.atspi.Registry=org.a11y.atspi.Socket.Unembed@/org/a11y/atspi/accessible/root",
+        "--call=org.a11y.atspi.Registry=org.a11y.atspi.Registry.GetRegisteredEvents@/org/a11y/atspi/registry",
+        "--call=org.a11y.atspi.Registry=org.a11y.atspi.DeviceEventController.GetKeystrokeListeners@/org/a11y/atspi/registry/deviceeventcontroller",
+        "--call=org.a11y.atspi.Registry=org.a11y.atspi.DeviceEventController.GetDeviceEventListeners@/org/a11y/atspi/registry/deviceeventcontroller",
+        "--call=org.a11y.atspi.Registry=org.a11y.atspi.DeviceEventController.NotifyListenersSync@/org/a11y/atspi/registry/deviceeventcontroller",
+        "--call=org.a11y.atspi.Registry=org.a11y.atspi.DeviceEventController.NotifyListenersAsync@/org/a11y/atspi/registry/deviceeventcontroller",
+    });
+
+    if (!g_strcmp0(g_getenv("WEBKIT_ENABLE_A11Y_DBUS_PROXY_LOGGING"), "1"))
+        m_args.append("--log");
+
+    return std::pair<CString, CString> { m_accessibilityProxyPath, m_accessibilityPath };
+#else
+    return std::nullopt;
+#endif
+}
+
+bool XDGDBusProxy::launch()
+{
+    if (m_syncFD)
+        return true;
+
+    if (m_args.isEmpty())
+        return false;
+
+    int syncFds[2];
+    if (pipe(syncFds) == -1)
+        g_error("Failed to make syncfds for dbus-proxy: %s", g_strerror(errno));
+    setCloseOnExec(syncFds[0]);
+
+    GUniquePtr<char> syncFdStr(g_strdup_printf("--fd=%d", syncFds[1]));
+    Vector<CString> proxyArgs = { syncFdStr.get() };
+    proxyArgs.appendVector(WTFMove(m_args));
+
     // We have to run xdg-dbus-proxy under bubblewrap because we need /.flatpak-info to exist in
     // xdg-dbus-proxy's mount namespace. Portals may use this as a trusted way to get the
     // sandboxed process's application ID, and will break if it's missing.
@@ -195,16 +199,14 @@ int XDGDBusProxy::launch(bool allowPortals) const
     GRefPtr<GSubprocessLauncher> launcher = adoptGRef(g_subprocess_launcher_new(G_SUBPROCESS_FLAGS_NONE));
     g_subprocess_launcher_take_fd(launcher.get(), proxyFd, proxyFd);
     g_subprocess_launcher_take_fd(launcher.get(), syncFds[1], syncFds[1]);
+    m_syncFD = UnixFileDescriptor(syncFds[0], UnixFileDescriptor::Adopt);
 
     // We are purposefully leaving syncFds[0] open here.
     // xdg-dbus-proxy will exit() itself once that is closed on our exit.
 
     ProcessLauncher::LaunchOptions launchOptions;
     launchOptions.processType = ProcessLauncher::ProcessType::DBusProxy;
-#if ENABLE(ACCESSIBILITY)
-    if (m_type == Type::AccessibilityBus && !m_path.isNull())
-        launchOptions.extraSandboxPaths.add(m_path, SandboxPermission::ReadOnly);
-#endif
+
     GUniqueOutPtr<GError> error;
     GRefPtr<GSubprocess> process = bubblewrapSpawn(launcher.get(), launchOptions, argv, &error.outPtr());
     if (!process)
@@ -216,7 +218,7 @@ int XDGDBusProxy::launch(bool allowPortals) const
     if (read(syncFds[0], &out, 1) != 1)
         g_error("Failed to fully launch dbus-proxy: %s", g_strerror(errno));
 
-    return syncFds[0];
+    return true;
 }
 
 } // namespace WebKit
diff --git a/Source/WebKit/UIProcess/Launcher/glib/XDGDBusProxy.h b/Source/WebKit/UIProcess/Launcher/glib/XDGDBusProxy.h
index 1d89346d79f5..76f537c0d435 100644
--- a/Source/WebKit/UIProcess/Launcher/glib/XDGDBusProxy.h
+++ b/Source/WebKit/UIProcess/Launcher/glib/XDGDBusProxy.h
@@ -28,29 +28,34 @@
 #if ENABLE(BUBBLEWRAP_SANDBOX)
 #include <wtf/FastMalloc.h>
 #include <wtf/Noncopyable.h>
+#include <wtf/Vector.h>
 #include <wtf/text/CString.h>
+#include <wtf/unix/UnixFileDescriptor.h>
 
 namespace WebKit {
 
 class XDGDBusProxy {
     WTF_MAKE_NONCOPYABLE(XDGDBusProxy); WTF_MAKE_FAST_ALLOCATED;
 public:
-    enum class Type { SessionBus, AccessibilityBus };
-    XDGDBusProxy(Type, bool = false);
-    ~XDGDBusProxy();
+    XDGDBusProxy() = default;
+    ~XDGDBusProxy() = default;
 
-    const CString& proxyPath() const { return m_proxyPath; }
-    const CString& path() const { return m_path; }
+    enum class AllowPortals : bool { No, Yes };
+    std::optional<std::pair<CString, CString>> dbusSessionProxy(AllowPortals);
+    std::optional<std::pair<CString, CString>> accessibilityProxy();
+
+    bool launch();
 
 private:
-    CString makeProxy() const;
-    int launch(bool) const;
-
-    Type m_type;
-    CString m_dbusAddress;
-    CString m_proxyPath;
-    CString m_path;
-    int m_syncFD { -1 };
+    static CString makePath(const char* dbusAddress);
+    static CString makeProxy(const char* proxyTemplate);
+
+    Vector<CString> m_args;
+    CString m_dbusSessionProxyPath;
+    CString m_dbusSessionPath;
+    CString m_accessibilityProxyPath;
+    CString m_accessibilityPath;
+    UnixFileDescriptor m_syncFD;
 };
 
 } // namespace WebKit
