# Contributor: Chloe Kudryavtsev <code@toast.bunkerlabs.net>
# Maintainer:
pkgname=procs
pkgver=0.13.2
pkgrel=0
pkgdesc="Modern replacement for ps written in Rust"
url="https://github.com/dalance/procs"
arch="all !s390x !riscv64" # limited by rust/cargo
license="MIT"
makedepends="cargo"
subpackages="
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="procs-$pkgver.tar.gz::https://github.com/dalance/procs/archive/v$pkgver.tar.gz"

export CARGO_PROFILE_RELEASE_CODEGEN_UNITS=1
export CARGO_PROFILE_RELEASE_LTO="true"
export CARGO_PROFILE_RELEASE_OPT_LEVEL="z"
export CARGO_PROFILE_RELEASE_PANIC="abort"

prepare() {
	default_prepare

	cargo fetch --locked
}

build() {
	# Default features enables "docker" feature that doubles the size of
	# the resulting binary with unnecessary bloat!
	cargo build --release --frozen --no-default-features

	# generate shell completions
	target/release/procs --completion bash
	target/release/procs --completion fish
	target/release/procs --completion zsh
}

check() {
	cargo test --frozen --no-default-features
}

package() {
	install -Dm755 target/release/procs "$pkgdir"/usr/bin/procs

	# install completions
	install -Dm644 procs.bash "$pkgdir"/usr/share/bash-completion/completions/procs
	install -Dm644 procs.fish "$pkgdir"/usr/share/fish/completions/procs.fish
	install -Dm644 _procs "$pkgdir"/usr/share/zsh/site-functions/_procs
}

sha512sums="
20e0f74791dd7f7afb5dc9a46bd9b1b4aaf6f2c52e909a198de86e027954831c6a6b2e693637bb3471979178da5deb33812dee8e1343c92b00198f100309e660  procs-0.13.2.tar.gz
"
