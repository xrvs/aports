# Contributor: George Hopkins <george-hopkins@null.net>
# Maintainer: George Hopkins <george-hopkins@null.net>
pkgname=imhex
pkgver=1.24.0
pkgrel=0
_patterns=665c50b9149c3b67209017e4100e9a16ef47cca3
pkgdesc="Hex editor for reverse engineers and programmers"
url="https://github.com/WerWolv/ImHex"
# armhf, armv7, x86: 32-bit not supported
# ppc64le: textrel in libromfs
arch="all !armhf !armv7 !ppc64le !x86"
license="GPL-2.0-or-later"
options="!check" # No testsuite
makedepends="
	capstone-dev
	cmake
	curl-dev
	file-dev
	fmt-dev
	freetype-dev
	glfw-dev
	glm-dev
	gtk+3.0-dev
	llvm-dev
	llvm-static
	mbedtls-dev
	nlohmann-json
	openssl-dev
	python3-dev
	samurai
	yara-dev
	"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/WerWolv/ImHex/releases/download/v$pkgver/Full.Sources.tar.gz
	imhex-patterns-$_patterns.tar.gz::https://github.com/WerWolv/ImHex-Patterns/archive/$_patterns.tar.gz
	"
builddir="$srcdir"/ImHex

prepare() {
	default_prepare

	mkdir -p "$builddir"/build/_deps
	mv "$srcdir"/ImHex-Patterns-$_patterns/ \
		"$builddir"/build/_deps/imhex_patterns-src

	sed -i 's|-Werror||g' \
		cmake/build_helpers.cmake \
		lib/external/pattern_language/lib/CMakeLists.txt
}

build() {
	# project cmake doesn't add llvm lib path correctly,
	# this is the easiest fix
	export LDFLAGS="$LDFLAGS -L$(llvm-config --libdir)"
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DUSE_SYSTEM_CURL=ON \
		-DUSE_SYSTEM_NLOHMANN_JSON=ON \
		-DUSE_SYSTEM_FMT=ON \
		-DUSE_SYSTEM_LLVM=ON \
		-DUSE_SYSTEM_YARA=ON \
		-DFETCHCONTENT_FULLY_DISCONNECTED=ON \
		-DIMHEX_STRIP_RELEASE=OFF
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
1badbfce31bbc0e823e643a562efa971690289449c624b08611b94ebeed69312c9aa74fa93efb4415ae0a758235a07e7dcec321a64e961ac93aae65ea23caa63  imhex-1.24.0.tar.gz
f3c0729c810530574988b9bc78555c0425621f260a28b6b7add10d989d0a0bbff4ff9bb815cb1ec7ebe90aa2a9abdd9cd33b3b5a6d63788ad61d09ffa5d2f705  imhex-patterns-665c50b9149c3b67209017e4100e9a16ef47cca3.tar.gz
"
